package com.sdmmllc.em.ui;

import java.lang.ref.WeakReference;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sdmmllc.em.Controller;
import com.sdmmllc.em.R;
import com.sdmmllc.em.data.ContactData;
import com.sdmmllc.em.data.DBAdapter;

public class ActProfile extends SherlockActivity {

	public static final String TAG = "ActProfile";

	private ImageView profileImg;
	private TextView meName, meHandle, emailTxt, phoneTxt, dateJoinedTxt;

	private TextView mHandle;
	private TextView mName;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_profile);
		SupActionBar.actionbar_contacts(new WeakReference<SherlockActivity>(this));

		final ContactData me = DBAdapter.getMeData();
		profileImg = (ImageView)findViewById(R.id.profileImage);
		if (DBAdapter.hasEmo(me.getUserEmoId())) {
			ImageLoader.getInstance().displayImage(DBAdapter.getEmo(me.getUserEmoId()).getLocation(),
					profileImg, Controller.avatarOptions);
		}
		
		meName = (TextView) findViewById(R.id.profileName);
		meName.setText(me.getUserName());
		meName.setTag(me.getUserId());
		
		meHandle = (TextView) findViewById(R.id.profileHandle);
		meHandle.setText(me.getHandle());

		emailTxt = (TextView) findViewById(R.id.profileEmail);
		phoneTxt = (TextView) findViewById(R.id.profilePhone);
		dateJoinedTxt = (TextView) findViewById(R.id.profileDate);
		dateJoinedTxt.setVisibility(View.GONE);
        SupActionBar.actionbar_contacts(new WeakReference<SherlockActivity>(this));
        mHandle = (TextView)findViewById(R.id.profileHandle);
        mName = (TextView)findViewById(R.id.profileName);
        
	}

	public void inviteFriends(View v) {
		Intent intent = new Intent(this, ActInvite.class);
		startActivity(intent);
	}
}