package com.sdmmllc.em.ui;

import java.lang.ref.WeakReference;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.sdmmllc.em.Controller;
import com.sdmmllc.em.R;
import com.sdmmllc.em.data.DBAdapter;

public class ActDictionary extends SherlockActivity {

	public static final String TAG = "ActDictionary";
	
	public static String EMO_ID = "emo_id";
	
	private ImageView emoImg, dictionaryEmo1, dictionaryEmo2, dictionaryEmo3, dictionaryEmo4;
	private TextView emoTitle, emoDescr, dictionaryExample, dictionaryExamplesTitle;
	private ScrollView dictionaryExamplesScrollView;
	private RelativeLayout dictionaryAdoptionLayout;
	private static ImageLoader loader;
	private Controller aController = null;
	
	private final double XXXHDPI = 4.0, 
			XXHDPI = 3.0, 
			XHDPI = 2.0, 
			HDPI = 1.5, 
			MDPI = 1.0,
			emoImgXXXHighDensityScale = 1.0664,
			emoImgXXHighDensityScale = 0.8,
			emoImgXHighDensityScale = 0.5336,
			emoImgHighDensityScale = 0.4,
			emoImgMediumDensityScale = 0.2664,
			emoImgLowDensityScale = 0.2,
			dictionaryEmoXXXHighDensityScale = 2.1336,
			dictionaryEmoXXHighDensityScale = 1.6,
			dictionaryEmoXHighDensityScale = 1.0664,
			dictionaryEmoHighDensityScale = 0.8,
			dictionaryEmoMediumDensityScale = 0.5336,
			dictionaryEmoLowDensityScale = 0.4;
	
	private double screenDensity = 0.0;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_dictionary);
		
		emoImg = (ImageView)findViewById(R.id.actDictionaryEmo);
		dictionaryEmo1 = (ImageView) findViewById(R.id.actDictionaryEmos1);
		dictionaryEmo2 = (ImageView) findViewById(R.id.actDictionaryEmos2);
		dictionaryEmo3 = (ImageView) findViewById(R.id.actDictionaryEmos3);
		dictionaryEmo4 = (ImageView) findViewById(R.id.actDictionaryEmos4);
		emoTitle = (TextView) findViewById(R.id.actDictionaryEmoTitle);
		emoDescr = (TextView) findViewById(R.id.actDictionaryEmoDescr);
		dictionaryExample = (TextView) findViewById(R.id.actDictionaryExampleText);
		dictionaryExamplesTitle = (TextView) findViewById(R.id.actDictionaryExamplesTitle);
		dictionaryExamplesScrollView = (ScrollView) findViewById(R.id.actDictionaryExamples);
		dictionaryAdoptionLayout = (RelativeLayout) findViewById(R.id.actDictionaryAdoptionLayout);
		dictionaryExamplesTitle.setVisibility(View.GONE);
		dictionaryExamplesScrollView.setVisibility(View.GONE);
		dictionaryAdoptionLayout.setVisibility(View.GONE);
		
        SupActionBar.actionbar_contacts(new WeakReference<SherlockActivity>(this));
        
        aController = Controller.getInstance();
        loader = Controller.getImageLoader();
        
        screenDensity = getResources().getDisplayMetrics().density;
        
        if (screenDensity >= XXXHDPI) {
        	RelativeLayout.LayoutParams emoImgParams = new RelativeLayout.LayoutParams(
        			(int) (getResources().getDrawable(R.drawable.emo_french_flag).getIntrinsicWidth()*emoImgXXXHighDensityScale),
        			(int) (getResources().getDrawable(R.drawable.emo_french_flag).getIntrinsicHeight()*emoImgXXXHighDensityScale));
        	emoImg.setLayoutParams(emoImgParams);
        	LinearLayout.LayoutParams dictionaryEmoParams = new LinearLayout.LayoutParams(
        			(int) (getResources().getDrawable(R.drawable.emoji_e423).getIntrinsicWidth()*dictionaryEmoXXXHighDensityScale),
        			(int) (getResources().getDrawable(R.drawable.emoji_e423).getIntrinsicHeight()*dictionaryEmoXXXHighDensityScale));
        	dictionaryEmo1.setLayoutParams(dictionaryEmoParams);
        	dictionaryEmo2.setLayoutParams(dictionaryEmoParams);
        	dictionaryEmo3.setLayoutParams(dictionaryEmoParams);
        	dictionaryEmo4.setLayoutParams(dictionaryEmoParams);
	    } else if (screenDensity >= XXHDPI) {
	    	RelativeLayout.LayoutParams emoImgParams = new RelativeLayout.LayoutParams(
        			(int) (getResources().getDrawable(R.drawable.emo_french_flag).getIntrinsicWidth()*emoImgXXHighDensityScale),
        			(int) (getResources().getDrawable(R.drawable.emo_french_flag).getIntrinsicHeight()*emoImgXXHighDensityScale));
        	emoImg.setLayoutParams(emoImgParams);
        	LinearLayout.LayoutParams dictionaryEmoParams = new LinearLayout.LayoutParams(
        			(int) (getResources().getDrawable(R.drawable.emoji_e423).getIntrinsicWidth()*dictionaryEmoXXHighDensityScale),
        			(int) (getResources().getDrawable(R.drawable.emoji_e423).getIntrinsicHeight()*dictionaryEmoXXHighDensityScale));
        	dictionaryEmo1.setLayoutParams(dictionaryEmoParams);
        	dictionaryEmo2.setLayoutParams(dictionaryEmoParams);
        	dictionaryEmo3.setLayoutParams(dictionaryEmoParams);
        	dictionaryEmo4.setLayoutParams(dictionaryEmoParams);
	    } else if (screenDensity >= XHDPI) {
	    	RelativeLayout.LayoutParams emoImgParams = new RelativeLayout.LayoutParams(
        			(int) (getResources().getDrawable(R.drawable.emo_french_flag).getIntrinsicWidth()*emoImgXHighDensityScale),
        			(int) (getResources().getDrawable(R.drawable.emo_french_flag).getIntrinsicHeight()*emoImgXHighDensityScale));
        	emoImg.setLayoutParams(emoImgParams);
        	LinearLayout.LayoutParams dictionaryEmoParams = new LinearLayout.LayoutParams(
        			(int) (getResources().getDrawable(R.drawable.emoji_e423).getIntrinsicWidth()*dictionaryEmoXHighDensityScale),
        			(int) (getResources().getDrawable(R.drawable.emoji_e423).getIntrinsicHeight()*dictionaryEmoXHighDensityScale));
        	dictionaryEmo1.setLayoutParams(dictionaryEmoParams);
        	dictionaryEmo2.setLayoutParams(dictionaryEmoParams);
        	dictionaryEmo3.setLayoutParams(dictionaryEmoParams);
        	dictionaryEmo4.setLayoutParams(dictionaryEmoParams);
	    } else if (screenDensity >= HDPI) {
	    	RelativeLayout.LayoutParams emoImgParams = new RelativeLayout.LayoutParams(
        			(int) (getResources().getDrawable(R.drawable.emo_french_flag).getIntrinsicWidth()*emoImgHighDensityScale),
        			(int) (getResources().getDrawable(R.drawable.emo_french_flag).getIntrinsicHeight()*emoImgHighDensityScale));
        	emoImg.setLayoutParams(emoImgParams);
        	LinearLayout.LayoutParams dictionaryEmoParams = new LinearLayout.LayoutParams(
        			(int) (getResources().getDrawable(R.drawable.emoji_e423).getIntrinsicWidth()*dictionaryEmoHighDensityScale),
        			(int) (getResources().getDrawable(R.drawable.emoji_e423).getIntrinsicHeight()*dictionaryEmoHighDensityScale));
        	dictionaryEmo1.setLayoutParams(dictionaryEmoParams);
        	dictionaryEmo2.setLayoutParams(dictionaryEmoParams);
        	dictionaryEmo3.setLayoutParams(dictionaryEmoParams);
        	dictionaryEmo4.setLayoutParams(dictionaryEmoParams);
	    } else if (screenDensity >= MDPI) {
	    	RelativeLayout.LayoutParams emoImgParams = new RelativeLayout.LayoutParams(
        			(int) (getResources().getDrawable(R.drawable.emo_french_flag).getIntrinsicWidth()*emoImgMediumDensityScale),
        			(int) (getResources().getDrawable(R.drawable.emo_french_flag).getIntrinsicHeight()*emoImgMediumDensityScale));
        	emoImg.setLayoutParams(emoImgParams);
        	LinearLayout.LayoutParams dictionaryEmoParams = new LinearLayout.LayoutParams(
        			(int) (getResources().getDrawable(R.drawable.emoji_e423).getIntrinsicWidth()*dictionaryEmoMediumDensityScale),
        			(int) (getResources().getDrawable(R.drawable.emoji_e423).getIntrinsicHeight()*dictionaryEmoMediumDensityScale));
        	dictionaryEmo1.setLayoutParams(dictionaryEmoParams);
        	dictionaryEmo2.setLayoutParams(dictionaryEmoParams);
        	dictionaryEmo3.setLayoutParams(dictionaryEmoParams);
        	dictionaryEmo4.setLayoutParams(dictionaryEmoParams);
	    } else {
	    	RelativeLayout.LayoutParams emoImgParams = new RelativeLayout.LayoutParams(
        			(int) (getResources().getDrawable(R.drawable.emo_french_flag).getIntrinsicWidth()*emoImgLowDensityScale),
        			(int) (getResources().getDrawable(R.drawable.emo_french_flag).getIntrinsicHeight()*emoImgLowDensityScale));
        	emoImg.setLayoutParams(emoImgParams);
        	LinearLayout.LayoutParams dictionaryEmoParams = new LinearLayout.LayoutParams(
        			(int) (getResources().getDrawable(R.drawable.emoji_e423).getIntrinsicWidth()*dictionaryEmoLowDensityScale),
        			(int) (getResources().getDrawable(R.drawable.emoji_e423).getIntrinsicHeight()*dictionaryEmoLowDensityScale));
        	dictionaryEmo1.setLayoutParams(dictionaryEmoParams);
        	dictionaryEmo2.setLayoutParams(dictionaryEmoParams);
        	dictionaryEmo3.setLayoutParams(dictionaryEmoParams);
        	dictionaryEmo4.setLayoutParams(dictionaryEmoParams);
	    }
        
        int emoId = getIntent().getIntExtra(EMO_ID, 0);
        loader.displayImage(aController.getEmoByCd(emoId).getLocation(), emoImg);
        
        emoTitle.setText(aController.getEmoByCd(emoId).getName());
        emoDescr.setText(aController.getEmoByCd(emoId).getDescr());
        
        loader.displayImage(aController.getEmoByCd(emoId).getLocation(), dictionaryEmo1);
        loader.displayImage(aController.getEmoByCd(emoId).getLocation(), dictionaryEmo2);
        loader.displayImage(aController.getEmoByCd(emoId).getLocation(), dictionaryEmo3);
        loader.displayImage(aController.getEmoByCd(emoId).getLocation(), dictionaryEmo4);
        dictionaryExample.setText("Under Construction at the Moment.");
	}

}