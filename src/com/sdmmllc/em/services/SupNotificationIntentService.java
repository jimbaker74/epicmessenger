package com.sdmmllc.em.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.sdmmllc.em.Controller;
import com.sdmmllc.em.data.DBAdapter;
import com.sdmmllc.em.receivers.SupBroadcastReceiver;

public class SupNotificationIntentService extends IntentService {
 
	public static final String TAG = "SupNotificationIntentService";
	
	public static final String SUP_ACTION = "com.sdmmllc.em.intent.RECEIVE";
	
    public SupNotificationIntentService() {
        super(TAG);
    }
 
    @Override
    protected void onHandleIntent(Intent intent) {

		int msgId = (int)intent.getLongExtra(Controller.MSG_ID, 0);

		Log.i(TAG, "received notification intent for: " + msgId);

		DBAdapter.setMsgIsResponding(msgId);
		
		SupBroadcastReceiver.completeWakefulIntent(intent);
    }
}