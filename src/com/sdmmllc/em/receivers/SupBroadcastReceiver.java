package com.sdmmllc.em.receivers;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.sdmmllc.em.Controller;
import com.sdmmllc.em.services.SupNotificationIntentService;
import com.sdmmllc.em.ui.ActSelectEmoList;

public class SupBroadcastReceiver extends WakefulBroadcastReceiver {
	
	public static final String TAG = "SupBroadcastReceiver";

	@Override
	public void onReceive(Context context, Intent intent) {	

		int msgId = (int)intent.getLongExtra(Controller.MSG_ID, 0);

		Log.i(TAG, "received notification intent for: " + msgId);

		if (intent.hasExtra("NOTIFICATION_ID")) {
			Log.i(TAG, "received SANotification intent for NOTIFICATION_ID: " + intent.getIntExtra("NOTIFICATION_ID", 0));
		}
		Intent actIntent = new Intent(context, ActSelectEmoList.class);
		actIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_SINGLE_TOP);
		actIntent.putExtra(Controller.MSG_ID, msgId);
		actIntent.putExtra(Controller.MSG_SENDTO_ID, intent.getLongExtra(Controller.MSG_SENDTO_ID, -1));
		context.startActivity(actIntent);

		ComponentName comp = new ComponentName(context.getPackageName(),
				SupNotificationIntentService.class.getName());
		startWakefulService(context, (intent.setComponent(comp)));
	}
}