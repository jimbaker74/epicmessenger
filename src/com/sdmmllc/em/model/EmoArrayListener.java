package com.sdmmllc.em.model;

import com.sdmmllc.em.data.EmoData;

public interface EmoArrayListener {
	public void onLoaded(EmoData emo);
}
